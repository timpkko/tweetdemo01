To connect to Twitter you will need the four keys you are assigned from dev.twitter.com. These must be entered into a properties file stored in Other Sources /src/main/resources. The file must be named "twitter4j.properties" and should contain the following four lines.

oauth.consumerKey =       
oauth.consumerSecret =    
oauth.accessToken =
oauth.accessTokenSecret = 

Add your keys after the equals sign.

### Relevant articles

- [Introduction to Twitter4J](http://www.baeldung.com/twitter4j)
